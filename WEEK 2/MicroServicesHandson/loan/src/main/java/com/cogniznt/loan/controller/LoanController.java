package com.cogniznt.loan.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cogniznt.loan.model.Loan;

@RestController
public class LoanController {
	public static ArrayList<Loan> lonn=new ArrayList<>();
	static
	{
		lonn.add(new Loan(12345,"car",400000,8000,18));
		lonn.add(new Loan(123456,"home",4000000,8000,12));
		lonn.add(new Loan(1234,"bike",40000,800,18));
	}
	@GetMapping("/loans/{number}")
	public Loan getLoanById(@PathVariable long num)
	{
		for(Loan l:lonn)
		{
			if(l.getNum()==num)
			{
				return l;
			}
		}
		return null;
	}

}
