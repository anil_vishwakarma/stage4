package com.cogniznt.account.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cogniznt.account.model.Account;

@RestController
public class AccountController {
	static ArrayList<Account> account1=new ArrayList<>();
	static
	{
		
		account1.add(new Account(123456,"saving",85686));
		account1.add(new Account(1235,"saving",43245));
		account1.add(new Account(1234,"saving",23234));
	}
	@GetMapping("/accounts/{number}")
	public Account getAcctByNum(@PathVariable long num)
	{
	       for(Account a:account1)
	       {
	    	   if(a.getNum()==num)
	    		   return a;
	       }
	        
	    	return   null;
	}
	

}
