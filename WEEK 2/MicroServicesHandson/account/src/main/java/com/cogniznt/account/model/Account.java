package com.cogniznt.account.model;


	public long num;
	public String type;
	public long bal;

	public class Account {
		
		public Account() {
			
		}
		
	public Account(long num, String type, long bal) {
		super();
		this.num = num;
		this.type = type;
		this.bal = bal;
	}
	
	public long getNum() {
		return num;
	}

	public void setNum(long num) {
		this.num = num;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public long getBal() {
		return bal;
	}

	public void setBal(long bal) {
		this.bal = bal;
	}

}
